### Welcome to Spring Street Securities.

Spring Street Securities is here to help you get started in algorithmic trading. If you get stuck contact us.

### Getting Started
We are assuming you have the following:
+ [Python 2.7 installed](https://www.python.org/downloads/)
+ [Basic Python skills](https://docs.python.org/2/tutorial/index.html)
+ [git installed](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

```
$ pip install pyalgotrade
$ git clone https://github.com/pinkgrass/Spring-Street-Securities.git
$ cd Spring-Street-Securities
$ python marketdata.py
$ python optimise.py strategy/rsi2.py marketdata/BTC.E2q_XRP-last-2160-hour-ripplecharts.csv
```
marketdata.py downloads the last 90days of trading activity for the most active Ripple Network markets
optimise.py back tests a simple trading algorithm over real market data from the Ripple Network

### Understanding the strategy
Take a look at strategy/sma_crossover.py. The algorithm is inside OnBars. If the price crosses above the simple moving average (sma) then we enter a long position. If the price crosses below the sma then we exit the market.

        def onBars(self, bars):
            # If a position was not opened, check if we should enter a long position.
            if self.__position is None:
                if cross.cross_above(self.__prices, self.__sma) > 0:
                    shares = int(self.getBroker().getCash() * 0.9 / bars[self.__instrument].getPrice())
                    # Enter a buy market order. The order is good till canceled.
                    self.__position = self.enterLong(self.__instrument, shares, True)
            # Check if we have to exit the position.
            elif not self.__position.exitActive() and cross.cross_below(self.__prices, self.__sma) > 0:
                self.__position.exitMarket()

### What Now?
You notice the strategy made a loss? Can you do better? Further reading we would suggest:
+ [Sample strategies for use in PyAlgo](http://gbeced.github.io/pyalgotrade/docs/v0.16/html/samples.html)
+ [Basic algos and concepts](http://www.investopedia.com/articles/active-trading/101014/basics-algorithmic-trading-concepts-and-examples.asp)
+ [Getting Started in Technical Analysis](http://www.amazon.co.uk/Getting-Started-Technical-Analysis-Schwager/dp/1118858530/ref=sr_1_2?ie=UTF8&qid=1430765789&sr=8-2&keywords=getting+started+in+technical+analysis)

### PyAlgoTrade
Spring Street Securities uses and recommends [PyAlgoTrade](http://gbeced.github.io/pyalgotrade/). PyAlgoTrade is a Python Algorithmic Trading Library with focus on backtesting and support for paper-trading and live-trading. Let’s say you have an idea for a trading strategy and you’d like to evaluate it with historical data and see how it behaves. PyAlgoTrade allows you to do so with minimal effort. Head over to [PyAlgo's extensive documentation and tutorials](http://gbeced.github.io/pyalgotrade/docs/v0.16/html/) for more information.

### Authors and Contributors
Like what you have seen then please @mention us and spread the word.

### Support or Contact
Having trouble? Contact [richard@pinkgrass.org](mailto:richard@pinkgrass.org) or raise an [issue](https://github.com/pinkgrass/Spring-Street-Securities/issues/new) and we’ll help you sort it out.
