# PyAlgoTrade - Local instrument backtest optimiser
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import itertools
import argparse
import inspect
import imp

import pyalgotrade.logger
from pyalgotrade.optimizer import local

from ripple import barfeed # generalise the barfeed 

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("strategyfile", help="strategy file", type=str)
    parser.add_argument("marketdatafile", help="market data file", type=str)
    args = parser.parse_args()
    strategyfile = args.strategyfile
    marketdatafile = args.marketdatafile
    logger.info('Strategy file %s' % (strategyfile))
    logger.info('Market data file %s' % (marketdatafile))
    return strategyfile,marketdatafile

def load_strategy(strategy_path):
    logger.info("Loading strategy source code from %s" % (strategy_path))
    strategy_module = imp.load_source('BacktestingStrategy',strategy_path)
    strategy_name, strategy_class = inspect.getmembers(strategy_module)[0] #loads first class found
    logger.info("Found strategy %s" % (strategy_name))
    return strategy_class

def load_market_data(marketdatafile):
    logger.info("Loading market data from %s" % (marketdatafile))
    feed = barfeed.Feed()  
    feed.addBarsFromCSV('instrument', marketdatafile)
    return feed

def parameters_generator(strategy):
    logger.info("Generating parameters")
    parameters = [['instrument']] + strategy.parameters()
    parameter_cnt = reduce(lambda x, y: x * y, [len(x) for x in parameters])
    logger.info("Found %d parameters" % (parameter_cnt))
    return itertools.product(*parameters)


if __name__ == '__main__':
    # python optimise.py strategy/rsi2.py marketdata/BTC.E2q_XRP-sample-ripplecharts.csv
    #
    # parse arguments, load strategy, load market data, generate parameters, backtest strategy
    #

    logger = pyalgotrade.logger.getLogger("optimiser")
    logger.info("Starting")
    
    strategyfile,marketdatafile = parse_arguments()
    #strategyfile,marketdatafile = 'strategy/rsi2.py','BTC.59B_XRP-last-2160-hour-ripplecharts.csv'

    strategy = load_strategy(strategyfile)
    feed = load_market_data(marketdatafile)

    parameters_iter = parameters_generator(strategy)

    results = local.run(strategy, feed, parameters_iter)

    best_parameter = results.getParameters()
    best_return = ((float(results.getResult())-1000000.0) / 1000000.0) * 100.0

    logger.info('Best parameters %s returning %s%%' % (str(best_parameter),str(best_return)))

    logger.info('Finishing')
